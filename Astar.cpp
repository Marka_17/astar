#include <iostream>
#include <vector>
#include <cmath>
#include <set>
#include <unordered_set>
#include <utility>
#include <ctime>

struct Node {
    int coordX, coordY;
    bool passability;
    mutable std::pair<int, int> parent;
    mutable double gValue = __INT_MAX__;
    Node() {}
    Node(const int &x, const int &y): coordX(x), coordY(y) {}
    Node(int x, int y, bool p): coordX(x), coordY(y), passability(p) {}
    friend std::ostream& operator <<(std::ostream &os, const Node &other) {
        os << "[" << other.coordX << ", " << other.coordY << "]";
        return os;
    }
    void operator= (const Node &other) {
        coordX = other.coordX;
        coordY = other.coordY;
        passability = other.passability;
    }
    bool operator == (const Node &other) const {
        return this->coordX == other.coordX && this->coordY == other.coordY;
    }
    bool operator != (const Node &other) const {
        return !(*this == other);
    }
};

// Euclidean distance
Node _finish = {4,4};
double heuristic(const Node &currentNode, const Node &finishNode) {
    return std::hypot(currentNode.coordX - finishNode.coordX,
                      currentNode.coordY - finishNode.coordY);
}

class Comp {
 public:
     bool operator () (const Node &first, const Node &second) {
         double fValueFirst = first.gValue + heuristic(first, _finish),
                fValueSecond = second.gValue + heuristic(second, _finish);
         if (fValueFirst == fValueSecond) {
             return heuristic(first, _finish) < heuristic(second, _finish);
         } else {
             return fValueFirst < fValueSecond;
         }
     }
};

// Initialization of graph
using Graph = std::vector<std::vector<Node>>;

Graph readGraph(int width, int height) {
    Graph g(width, std::vector<Node>(height));
    for (size_t i = 0; i != g.size(); ++i) {
        for (size_t j = 0; j != g[i].size(); ++j) {
            bool passability;
            std::cin >> passability;
            Node temp(i, j, passability);
            g[i][j] = temp;
        }
    }
    return g;
}

// Vector of adjacent nodes for any node
std::vector<Node> adjacentNode(const Node &node, const Graph &g) {
    std::vector<Node> adjacentNodes;
    for (short int x = -1; x <= 1; ++x) {
        for (short int y = -1; y <= 1; ++y) {
            if (y == 0 && x == 0)
                continue;
            int tempX = node.coordX + x, tempY = node.coordY + y;
            if (tempX < g.size() && 0 <= tempX && tempY < g[0].size() && 0 <= tempY) {
                if (g[tempX][tempY].passability == false) {
                    adjacentNodes.push_back({tempX, tempY});
                }
            }
        }
    }
    return adjacentNodes;
}

// Hash-function for std::unordered_set<Node>
namespace std {
    template <>
    struct hash<Node> {
        std::size_t operator()(const Node &other) const {
            std::size_t const h1 (std::hash<int>()(other.coordX));
            std::size_t const h2 (std::hash<int>()(other.coordY));
            return h1 ^ (h2 << 1);
        }
    };
}

// Astar algorithm
std::unordered_set<Node> astar(const Graph &g, const Node &start, const Node &finish) {
    _finish = finish; // for definition of heuristic function
    Node _start = start;
    _start.gValue = 0;
    std::set<Node, Comp> opened{_start};
    std::unordered_set<Node> closed;
    while (!opened.empty()) {
        auto current = *opened.begin();
        opened.erase(opened.begin());
        if (closed.find(current) != closed.end()) {
            continue;
        } else {
            closed.insert(current);
        }
        if (current == finish) {
            break;
        }
        for (auto c : adjacentNode(current, g)) {
            if (closed.find(c) == closed.end()) {
                if (c.gValue > current.gValue + heuristic(current, c))   { // use hueristic function for finding
                    c.gValue = current.gValue + heuristic(current, c);     // distance between current and c nodes
                    c.parent = std::make_pair(current.coordX, current.coordY);

                    auto it = std::find(opened.begin(), opened.end(), c);
                    if (it == opened.end()) {
                        opened.insert(c);
                    } else {
                        it->gValue = c.gValue;
                        it->parent = c.parent;
                    }
                }
            }
        }
    }
    return closed;
}

std::vector<Node> reconstructionPath(const Node &start, const Node &finish,
                                     const std::unordered_set<Node> &closed) {
    std::vector<Node> path;
    auto current = finish;
    while (current != start) {
        path.push_back(current);
        auto temp = closed.find(current)->parent;
        current.coordX = temp.first;
        current.coordY = temp.second;
    }
    path.push_back(start);
    return path;
}

int main() {
    // auto start = clock();
    // START OF INITIALIZATION
    int width, height, startX, startY, finishX, finishY;
    std::cin >> width >> height >> startX >> startY >> finishX >> finishY;
    Node startPosition(startX, startY),
         finishPosition(finishX, finishY);
    Graph g = readGraph(width, height);
    // FINISH OF INITIALIZATION
    Node temp(4, 1);
    for (auto elem : adjacentNode(temp, g)) {
        std::cout << elem << " ";
    }
    auto closed = astar(g, startPosition, finishPosition);
    auto finishNode = closed.find(finishPosition);
    if (finishNode != closed.end()) {
        std::cout << "Length path : " << finishNode->gValue << std::endl;
        std::cout << "Path : " << std::endl;
        for (auto coord : reconstructionPath(startPosition, finishPosition, closed)) {
            std::cout << coord << std::endl;
        }
    } else {
        std::cout << "Path doesn't exist" << std::endl;
    }
    /* auto end = clock();
    std::cout << static_cast<float>(end - start) / CLOCKS_PER_SEC << std::endl; */
    return 0;
}
